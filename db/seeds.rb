# frozen_string_literal: true

puts 'creating active migration'
active_migration = Labm::Migration.create!(
  started_at: Time.current,
  min_value: 1,
  max_value: 100,
  total_tuple_count: 100,
  batch_size: 20,
  sub_batch_size: 5,
  interval: 2.minutes,
  status: 1,
  job_class_name: 'TestJob',
  table_name: 'test_table',
  column_name: 'id')

puts 'creating succeeded migration job'
succeeded_migration_job = Labm::MigrationJob.create!(
  started_at: Time.current - 5.minutes,
  finished_at: Time.current - 4.minutes,
  migration: active_migration,
  min_value: 1,
  max_value: 20,
  batch_size: 20,
  sub_batch_size: 5,
  status: 3,
  attempts: 1,
  metrics: { update_all: ['1.0', '0.5', '0.7', '0.9'] }
)

puts 'creating failed migration job'
failed_migration_job = Labm::MigrationJob.create!(
  started_at: Time.current - 2.minutes,
  finished_at: Time.current - 1.minute,
  migration: active_migration,
  min_value: 21,
  max_value: 40,
  batch_size: 20,
  sub_batch_size: 5,
  status: 2,
  attempts: 3,
  metrics: {}
)

