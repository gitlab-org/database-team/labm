class CreateMigrationTables < ActiveRecord::Migration[6.1]
  def change
    create_table :batched_background_migrations do |t|
      t.column :created_at, :timestamptz, null: false
      t.column :updated_at, :timestamptz, null: false
      t.bigint :min_value, null: false, default: 1
      t.bigint :max_value, null: false
      t.integer :batch_size, null: false
      t.integer :sub_batch_size, null: false
      t.integer :interval, null: false, limit: 2
      t.integer :status, null: false, limit: 2, default: 0
      t.text :job_class_name, null: false
      t.text :batch_class_name, null: false, default: 'PrimaryKeyBatchingStrategy'
      t.text :table_name, null: false
      t.text :column_name, null: false
      t.jsonb :job_arguments, null: false, default: []
      t.bigint :total_tuple_count
      t.integer :pause_ms, null: false, default: 100
      t.integer :max_batch_size
      t.column :started_at, :timestamptz
      t.column :on_hold_until, :timestamptz, comment: 'execution of this migration is on hold until this time'

      t.check_constraint 'char_length(column_name) <= 63', name: 'check_5bb0382d6f'
      t.check_constraint 'char_length(table_name) <= 63', name: 'check_6b6a06254a'
      t.check_constraint 'batch_size >= sub_batch_size', name: 'check_batch_size_in_range'
      t.check_constraint 'char_length(job_class_name) <= 100', name: 'check_e6c75b1e29'
      t.check_constraint 'char_length(batch_class_name) <= 100', name: 'check_fe10674721'
      t.check_constraint 'max_value >= min_value', name: 'check_max_value_in_range'
      t.check_constraint 'min_value > 0', name: 'check_positive_min_value'
      t.check_constraint 'sub_batch_size > 0', name: 'check_positive_sub_batch_size'

      t.index [:job_class_name, :table_name, :column_name, :job_arguments], unique: true,
        name: 'index_batched_background_migrations_on_unique_configuration'
      t.index :status, name: 'index_batched_background_migrations_on_status'
    end

    create_table :batched_background_migration_jobs do |t|
      t.column :created_at, :timestamptz, null: false
      t.column :updated_at, :timestamptz, null: false
      t.column :started_at, :timestamptz
      t.column :finished_at, :timestamptz
      t.references :batched_background_migration, null: false, foreign_key: { on_delete: :cascade }, index: false
      t.bigint :min_value, null: false
      t.bigint :max_value, null: false
      t.integer :batch_size, null: false
      t.integer :sub_batch_size, null: false
      t.integer :status, null: false, limit: 2, default: 0
      t.integer :attempts, null: false, limit: 2, default: 0
      t.jsonb :metrics, null: false, default: {}
      t.integer :pause_ms, null: false, default: 100

      t.index [:batched_background_migration_id, :id], name: 'index_batched_jobs_by_batched_migration_id_and_id'
      t.index [:batched_background_migration_id, :status], name: 'index_batched_jobs_on_batched_migration_id_and_status'
      t.index [:batched_background_migration_id, :finished_at], name: 'index_migration_jobs_on_migration_id_and_finished_at'
      t.index [:batched_background_migration_id, :max_value], name: 'index_migration_jobs_on_migration_id_and_max_value'
    end

    create_table :batched_background_migration_job_transition_logs, primary_key: [:id, :created_at] do |t|
      t.bigserial :id, null: false
      t.column :created_at, :timestamptz, null: false
      t.column :updated_at, :timestamptz, null: false
      t.references :batched_background_migration_job, null: false, foreign_key: { on_delete: :cascade }, index: { name: 'i_batched_background_migration_job_transition_logs_on_job_id' }
      t.integer :previous_status, null: false, limit: 2
      t.integer :next_status, null: false, limit: 2
      t.text :exception_class
      t.text :exception_message

      t.check_constraint 'char_length(exception_class) <= 1000', name: "check_76e202c37a"
      t.check_constraint 'char_length(exception_message) <= 1000', name: "check_50e580811a"
    end
  end
end
