# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Labm::MigrationHelpers do
  let(:connection) { Labm::ApplicationRecord.connection }
  let(:migration) { ActiveRecord::Migration.new.extend(described_class) }

  let(:events) do
    Class.new(Labm::ApplicationRecord) do
      self.table_name = :events

      def self.name
        'Event'
      end
    end
  end

  before do
    connection.create_table :events do |t|
      t.column :created_at, :timestamptz, null: false
      t.column :updated_at, :timestamptz, null: false
      t.text :name
    end
  end

  describe '#queue_batched_background_migration' do
    context 'when such migration already exists' do
      it 'does not create duplicate migration' do
        create(
          :labm_migration,
          job_class_name: 'MyJobClass',
          table_name: :events,
          column_name: :id,
          interval: 10.minutes,
          min_value: 5,
          max_value: 1005,
          batch_class_name: 'MyBatchClass',
          batch_size: 200,
          sub_batch_size: 20,
          job_arguments: [[:id], [:id_convert_to_bigint]]
        )

        expect do
          migration.queue_batched_background_migration(
            'MyJobClass',
            :events,
            :id,
            [:id], [:id_convert_to_bigint],
            job_interval: 5.minutes,
            batch_min_value: 5,
            batch_max_value: 1000,
            batch_class_name: 'MyBatchClass',
            batch_size: 100,
            sub_batch_size: 10)
        end.not_to change { Labm::Migration.count }
      end
    end

    it 'creates the database record for the migration' do
      expect(migration).to receive(:estimated_tuple_count).with(:events).and_return(5)

      expect do
        migration.queue_batched_background_migration(
          'MyJobClass',
          :events,
          :id,
          job_interval: 5.minutes,
          batch_min_value: 5,
          batch_max_value: 1000,
          batch_class_name: 'MyBatchClass',
          batch_size: 100,
          max_batch_size: 10000,
          sub_batch_size: 10)
      end.to change { Labm::Migration.count }.by(1)

      expect(Labm::Migration.last).to have_attributes(
        job_class_name: 'MyJobClass',
        table_name: 'events',
        column_name: 'id',
        interval: 300,
        min_value: 5,
        max_value: 1000,
        batch_class_name: 'MyBatchClass',
        batch_size: 100,
        max_batch_size: 10000,
        sub_batch_size: 10,
        job_arguments: %w[],
        status_name: :active,
        total_tuple_count: 5)
    end

    context 'when the job interval is lower than the minimum' do
      let(:minimum_delay) { described_class::BATCH_MIN_DELAY }

      it 'sets the job interval to the minimum value' do
        expect do
          migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: minimum_delay - 1.minute)
        end.to change { Labm::Migration.count }.by(1)

        created_migration = Labm::Migration.last

        expect(created_migration.interval).to eq(minimum_delay)
      end
    end

    context 'when additional arguments are passed to the method' do
      it 'saves the arguments on the database record' do
        expect do
          migration.queue_batched_background_migration(
            'MyJobClass',
            :events,
            :id,
            'my',
            'arguments',
            job_interval: 5.minutes,
            batch_max_value: 1000)
        end.to change { Labm::Migration.count }.by(1)

        expect(Labm::Migration.last).to have_attributes(
          job_class_name: 'MyJobClass',
          table_name: 'events',
          column_name: 'id',
          interval: 300,
          min_value: 1,
          max_value: 1000,
          job_arguments: %w[my arguments])
      end
    end

    context 'when the max_value is not given' do
      context 'when records exist in the database' do
        let!(:event1) { events.create!(name: 'event1') }
        let!(:event2) { events.create!(name: 'event1') }
        let!(:event3) { events.create!(name: 'event1') }

        it 'creates the record with the current max value' do
          expect do
            migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: 5.minutes)
          end.to change { Labm::Migration.count }.by(1)

          created_migration = Labm::Migration.last

          expect(created_migration.max_value).to eq(event3.id)
        end

        it 'creates the record with an active status' do
          expect do
            migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: 5.minutes)
          end.to change { Labm::Migration.count }.by(1)

          expect(Labm::Migration.last).to be_active
        end
      end

      context 'when the database is empty' do
        it 'sets the max value to the min value' do
          expect do
            migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: 5.minutes)
          end.to change { Labm::Migration.count }.by(1)

          created_migration = Labm::Migration.last

          expect(created_migration.max_value).to eq(created_migration.min_value)
        end

        it 'does not set the total_tuple_count' do
          expect(migration).to receive(:estimated_tuple_count).with(:events).and_return(0)

          expect do
            migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: 5.minutes)
          end.to change { Labm::Migration.count }.by(1)

          created_migration = Labm::Migration.last

          expect(created_migration.total_tuple_count).to be_nil
        end

        it 'creates the record with a finished status' do
          expect do
            migration.queue_batched_background_migration('MyJobClass', :events, :id, job_interval: 5.minutes)
          end.to change { Labm::Migration.count }.by(1)

          expect(Labm::Migration.last).to be_finished
        end
      end
    end
  end
end

