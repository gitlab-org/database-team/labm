# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Labm::MigrationRunner do
  let(:connection) { Labm::ApplicationRecord.connection }
  let(:migration_wrapper) { instance_double('Labm::MigrationWrapper') }

  let(:runner) { described_class.new(connection: connection, migration_wrapper: migration_wrapper) }

  describe '#run_migration_job' do
    let(:events) do
      Class.new(Labm::ApplicationRecord) do
        self.table_name = :events

        def self.name
          'Event'
        end
      end
    end

    before do
      connection.create_table :events do |t|
        t.column :created_at, :timestamptz, null: false
        t.column :updated_at, :timestamptz, null: false
        t.text :name
      end
    end

    shared_examples_for 'it has completed the migration' do
      it 'does not create and run a migration job' do
        expect(migration_wrapper).not_to receive(:perform)

        expect do
          runner.run_migration_job(migration)
        end.not_to change { Labm::MigrationJob.count }
      end

      it 'marks the migration as finished' do
        runner.run_migration_job(migration)

        expect(migration.reload).to be_finished
      end
    end

    context 'when the migration has no previous jobs' do
      let(:migration) { create(:labm_migration, :active, batch_size: 2) }

      let(:job_relation) do
        Labm::MigrationJob.where(migration: migration)
      end

      context 'when the migration has batches to process' do
        let!(:event1) { events.create!(name: 'event1') }
        let!(:event2) { events.create!(name: 'event2') }
        let!(:event3) { events.create!(name: 'event3') }

        it 'runs the job for the first batch' do
          migration.update!(min_value: event1.id, max_value: event2.id)

          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(job_relation.first)
          end

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(1)

          expect(job_relation.first).to have_attributes(
            min_value: event1.id,
            max_value: event2.id,
            batch_size: migration.batch_size,
            sub_batch_size: migration.sub_batch_size)
        end

        it 'optimizes the migration after executing the job' do
          migration.update!(min_value: event1.id, max_value: event2.id)

          expect(migration_wrapper).to receive(:perform).ordered
          expect(migration).to receive(:optimize!).ordered

          runner.run_migration_job(migration)
        end
      end

      context 'when the batch maximum exceeds the migration maximum' do
        let!(:event1) { events.create!(name: 'event1') }
        let!(:event2) { events.create!(name: 'event2') }
        let!(:event3) { events.create!(name: 'event3') }

        it 'clamps the batch maximum to the migration maximum' do
          migration.update!(min_value: event1.id, max_value: event2.id, batch_size: 5)

          expect(migration_wrapper).to receive(:perform)

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(1)

          expect(job_relation.first).to have_attributes(
            min_value: event1.id,
            max_value: event2.id,
            batch_size: migration.batch_size,
            sub_batch_size: migration.sub_batch_size)
        end
      end

      context 'when the migration has no batches to process' do
        it_behaves_like 'it has completed the migration'
      end
    end

    context 'when the migration should stop' do
      let(:migration) { create(:labm_migration, :active) }

      let!(:job) { create(:labm_migration_job, :failed, migration: migration) }

      it 'changes the status to failure' do
        expect(migration).to receive(:should_stop?).and_return(true)
        expect(migration_wrapper).to receive(:perform).and_return(job)

        expect { runner.run_migration_job(migration) }.to change { migration.status_name }.from(:active).to(:failed)
      end
    end

    context 'when the migration has previous jobs' do
      let!(:event1) { events.create!(name: 'event1') }
      let!(:event2) { events.create!(name: 'event2') }
      let!(:event3) { events.create!(name: 'event3') }

      let!(:migration) do
        create(:labm_migration, :active, batch_size: 2, min_value: event1.id, max_value: event2.id)
      end

      let!(:previous_job) do
        create(:labm_migration_job, :succeeded,
          migration: migration,
          min_value: event1.id,
          max_value: event2.id,
          batch_size: 2,
          sub_batch_size: 1
        )
      end

      let(:job_relation) do
        Labm::MigrationJob.where(migration: migration)
      end

      context 'when the migration has no batches remaining' do
        it_behaves_like 'it has completed the migration'
      end

      context 'when the migration has batches to process' do
        before do
          migration.update!(max_value: event3.id)
        end

        it 'runs the migration job for the next batch' do
          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(job_relation.last)
          end

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(1)

          expect(job_relation.last).to have_attributes(
            min_value: event3.id,
            max_value: event3.id,
            batch_size: migration.batch_size,
            sub_batch_size: migration.sub_batch_size)
        end

        context 'when the batch minimum exceeds the migration maximum' do
          before do
            migration.update!(batch_size: 5, max_value: event2.id)
          end

          it_behaves_like 'it has completed the migration'
        end
      end

      context 'when migration has failed jobs' do
        before do
          previous_job.failure!
        end

        it 'retries the failed job' do
          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(previous_job)
          end

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(0)
        end

        context 'when failed job has reached the maximum number of attempts' do
          before do
            previous_job.update!(attempts: Labm::MigrationJob::MAX_ATTEMPTS)
          end

          it 'marks the migration as failed' do
            expect(migration_wrapper).not_to receive(:perform)

            expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(0)

            expect(migration).to be_failed
          end
        end
      end

      context 'when migration has stuck jobs' do
        before do
          previous_job.update!(status_event: 'run', updated_at: 1.hour.ago - Labm::MigrationJob::STUCK_JOBS_TIMEOUT)
        end

        it 'retries the stuck job' do
          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(previous_job)
          end

          expect { runner.run_migration_job(migration.reload) }.to change { job_relation.count }.by(0)
        end
      end

      context 'when migration has possible stuck jobs' do
        before do
          previous_job.update!(status_event: 'run', updated_at: 1.hour.from_now - Labm::MigrationJob::STUCK_JOBS_TIMEOUT)
        end

        it 'keeps the migration active' do
          expect(migration_wrapper).not_to receive(:perform)

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(0)

          expect(migration.reload).to be_active
        end
      end

      context 'when the migration has batches to process and failed jobs' do
        before do
          migration.update!(max_value: event3.id)
          previous_job.failure!
        end

        it 'runs next batch then retries the failed job' do
          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(job_relation.last)
            job_record.succeed!
          end

          expect { runner.run_migration_job(migration) }.to change { job_relation.count }.by(1)

          expect(migration_wrapper).to receive(:perform) do |job_record|
            expect(job_record).to eq(previous_job)
          end

          expect { runner.run_migration_job(migration.reload) }.to change { job_relation.count }.by(0)
        end
      end
    end
  end

  describe '#run_entire_migration' do
    let(:events) do
      Class.new(Labm::ApplicationRecord) do
        self.table_name = :events

        def self.name
          'Event'
        end
      end
    end

    before do
      connection.create_table :events do |t|
        t.column :created_at, :timestamptz, null: false
        t.column :updated_at, :timestamptz, null: false
        t.text :name
      end
    end

    context 'when not in a development or test environment' do
      it 'raises an error' do
        environment = double('environment', development?: false, test?: false)
        migration = build(:labm_migration, :finished)

        allow(Rails).to receive(:env).and_return(environment)

        expect do
          runner.run_entire_migration(migration)
        end.to raise_error('this method is not intended for use in real environments')
      end
    end

    context 'when the given migration is not active' do
      it 'does not create and run migration jobs' do
        migration = build(:labm_migration, :finished)

        expect(migration_wrapper).not_to receive(:perform)

        expect do
          runner.run_entire_migration(migration)
        end.not_to change { Labm::MigrationJob.count }
      end
    end

    context 'when the given migration is active' do
      let!(:event1) { events.create!(name: 'event1') }
      let!(:event2) { events.create!(name: 'event2') }
      let!(:event3) { events.create!(name: 'event3') }

      let!(:migration) do
        create(:labm_migration, :active, batch_size: 2, min_value: event1.id, max_value: event3.id)
      end

      let(:job_relation) do
        Labm::MigrationJob.where(migration: migration)
      end

      it 'runs all jobs inline until finishing the migration' do
        expect(migration_wrapper).to receive(:perform) do |job_record|
          expect(job_record).to eq(job_relation.first)
          job_record.succeed!
        end

        expect(migration_wrapper).to receive(:perform) do |job_record|
          expect(job_record).to eq(job_relation.last)
          job_record.succeed!
        end

        expect { runner.run_entire_migration(migration) }.to change { job_relation.count }.by(2)

        expect(job_relation.first).to have_attributes(min_value: event1.id, max_value: event2.id)
        expect(job_relation.last).to have_attributes(min_value: event3.id, max_value: event3.id)

        expect(migration.reload).to be_finished
      end
    end
  end

  describe '#finalize' do
    let(:migration_wrapper) do
      Labm::MigrationWrapper.new(connection: connection)
    end

    let(:migration_helpers) { ActiveRecord::Migration.new }
    let(:table_name) { :_test_batched_migrations_test_table }
    let(:column_name) { :some_id }
    let(:job_arguments) { [:some_id, :some_id_convert_to_bigint] }

    let(:migration_status) { :active }

    let!(:migration) do
      create(:labm_migration, migration_status,
        max_value: 8,
        batch_size: 2,
        sub_batch_size: 1,
        interval: 0,
        table_name: table_name,
        column_name: column_name,
        job_arguments: job_arguments,
        pause_ms: 0
      )
    end

    before do
      migration_helpers.drop_table table_name, if_exists: true
      migration_helpers.create_table table_name, id: false do |t|
        t.integer :some_id, primary_key: true
        t.integer :some_id_convert_to_bigint
      end

      migration_helpers.execute("INSERT INTO #{table_name} VALUES (1, 1), (2, 2), (3, NULL), (4, NULL), (5, NULL), (6, NULL), (7, NULL), (8, NULL)")
    end

    after do
      migration_helpers.drop_table table_name, if_exists: true
    end

    context 'when the migration is not yet completed' do
      before do
        common_attributes = {
          migration: migration,
          batch_size: 2,
          sub_batch_size: 1,
          pause_ms: 0
        }

        create(:labm_migration_job, :succeeded, common_attributes.merge(min_value: 1, max_value: 2))
        create(:labm_migration_job, :pending, common_attributes.merge(min_value: 3, max_value: 4))
        create(:labm_migration_job, :failed, common_attributes.merge(min_value: 5, max_value: 6, attempts: 1))
      end

      it 'completes the migration' do
        expect(Labm::Migration).to receive(:find_for_configuration)
          .with('CopyColumn', table_name, column_name, job_arguments)
          .and_return(migration)

        expect(migration).to receive(:finalize!).and_call_original

        expect do
          runner.finalize(
            migration.job_class_name,
            table_name,
            column_name,
            job_arguments
          )
        end.to change { migration.reload.status_name }.from(:active).to(:finished)

        expect(migration.migration_jobs).to all(be_succeeded)

        not_converted = migration_helpers.execute("SELECT * FROM #{table_name} WHERE some_id_convert_to_bigint IS NULL")
        expect(not_converted.to_a).to be_empty
      end

      context 'when migration fails to complete' do
        it 'raises an error' do
          migration.migration_jobs.with_status(:failed).update_all(attempts: Labm::MigrationJob::MAX_ATTEMPTS)

          expect do
            runner.finalize(
              migration.job_class_name,
              table_name,
              column_name,
              job_arguments
            )
          end.to raise_error described_class::FailedToFinalize
        end
      end
    end

    context 'when the migration is already finished' do
      let(:migration_status) { :finished }

      it 'is a no-op' do
        expect(Labm::Migration).to receive(:find_for_configuration)
          .with('CopyColumn', table_name, column_name, job_arguments)
          .and_return(migration)

        configuration = {
          job_class_name: migration.job_class_name,
          table_name: table_name.to_sym,
          column_name: column_name.to_sym,
          job_arguments: job_arguments
        }

        expect(Labm.logger).to receive(:warn)
          .with("Background migration for the given configuration is already finished: #{configuration}")

        expect(migration).not_to receive(:finalize!)

        runner.finalize(
          migration.job_class_name,
          table_name,
          column_name,
          job_arguments
        )
      end
    end

    context 'when the migration does not exist' do
      it 'is a no-op' do
        expect(Labm::Migration).to receive(:find_for_configuration)
          .with('CopyColumn', table_name, column_name, [:some, :other, :arguments])
          .and_return(nil)

        configuration = {
          job_class_name: migration.job_class_name,
          table_name: table_name.to_sym,
          column_name: column_name.to_sym,
          job_arguments: [:some, :other, :arguments]
        }

        expect(Labm.logger).to receive(:warn)
          .with("Could not find background migration for the given configuration: #{configuration}")

        expect(migration).not_to receive(:finalize!)

        runner.finalize(
          migration.job_class_name,
          table_name,
          column_name,
          [:some, :other, :arguments]
        )
      end
    end
  end

  describe '.finalize' do
    context 'when the connection is passed' do
      let(:table_name) { :_test_batched_migrations_test_table }
      let(:column_name) { :some_id }
      let(:job_arguments) { [:some, :other, :arguments] }
      let(:migration) { create(:labm_migration, table_name: table_name, column_name: column_name) }

      it 'initializes the object with the given connection' do
        expect(described_class).to receive(:new).with(connection: connection).and_call_original

        described_class.finalize(
          migration.job_class_name,
          table_name,
          column_name,
          job_arguments,
          connection: connection
        )
      end
    end
  end
end
