# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_14_153121) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "batched_background_migration_job_transition_logs", primary_key: ["id", "created_at"], force: :cascade do |t|
    t.bigserial "id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "batched_background_migration_job_id", null: false
    t.integer "previous_status", limit: 2, null: false
    t.integer "next_status", limit: 2, null: false
    t.text "exception_class"
    t.text "exception_message"
    t.index ["batched_background_migration_job_id"], name: "i_batched_background_migration_job_transition_logs_on_job_id"
    t.check_constraint "char_length(exception_class) <= 1000", name: "check_76e202c37a"
    t.check_constraint "char_length(exception_message) <= 1000", name: "check_50e580811a"
  end

  create_table "batched_background_migration_jobs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "started_at"
    t.datetime "finished_at"
    t.bigint "batched_background_migration_id", null: false
    t.bigint "min_value", null: false
    t.bigint "max_value", null: false
    t.integer "batch_size", null: false
    t.integer "sub_batch_size", null: false
    t.integer "status", limit: 2, default: 0, null: false
    t.integer "attempts", limit: 2, default: 0, null: false
    t.jsonb "metrics", default: {}, null: false
    t.integer "pause_ms", default: 100, null: false
    t.index ["batched_background_migration_id", "finished_at"], name: "index_migration_jobs_on_migration_id_and_finished_at"
    t.index ["batched_background_migration_id", "id"], name: "index_batched_jobs_by_batched_migration_id_and_id"
    t.index ["batched_background_migration_id", "max_value"], name: "index_migration_jobs_on_migration_id_and_max_value"
    t.index ["batched_background_migration_id", "status"], name: "index_batched_jobs_on_batched_migration_id_and_status"
  end

  create_table "batched_background_migrations", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "min_value", default: 1, null: false
    t.bigint "max_value", null: false
    t.integer "batch_size", null: false
    t.integer "sub_batch_size", null: false
    t.integer "interval", limit: 2, null: false
    t.integer "status", limit: 2, default: 0, null: false
    t.text "job_class_name", null: false
    t.text "batch_class_name", default: "PrimaryKeyBatchingStrategy", null: false
    t.text "table_name", null: false
    t.text "column_name", null: false
    t.jsonb "job_arguments", default: [], null: false
    t.bigint "total_tuple_count"
    t.integer "pause_ms", default: 100, null: false
    t.integer "max_batch_size"
    t.datetime "started_at"
    t.datetime "on_hold_until", comment: "execution of this migration is on hold until this time"
    t.index ["job_class_name", "table_name", "column_name", "job_arguments"], name: "index_batched_background_migrations_on_unique_configuration", unique: true
    t.index ["status"], name: "index_batched_background_migrations_on_status"
    t.check_constraint "batch_size >= sub_batch_size", name: "check_batch_size_in_range"
    t.check_constraint "char_length(batch_class_name) <= 100", name: "check_fe10674721"
    t.check_constraint "char_length(column_name) <= 63", name: "check_5bb0382d6f"
    t.check_constraint "char_length(job_class_name) <= 100", name: "check_e6c75b1e29"
    t.check_constraint "char_length(table_name) <= 63", name: "check_6b6a06254a"
    t.check_constraint "max_value >= min_value", name: "check_max_value_in_range"
    t.check_constraint "min_value > 0", name: "check_positive_min_value"
    t.check_constraint "sub_batch_size > 0", name: "check_positive_sub_batch_size"
  end

  add_foreign_key "batched_background_migration_job_transition_logs", "batched_background_migration_jobs", on_delete: :cascade
  add_foreign_key "batched_background_migration_jobs", "batched_background_migrations", on_delete: :cascade
end
