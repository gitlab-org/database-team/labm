require_relative "lib/labm/version"

Gem::Specification.new do |spec|
  spec.name        = "labm"
  spec.version     = Labm::Version::VERSION
  spec.authors     = ["pbair"]
  spec.email       = ["pbair@gitlab.com"]
  spec.homepage    = "https://gitlab.com/gitlab-org/labm"
  spec.summary     = "GitLab migration engine"
  spec.description = "GitLab migration engine"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gitlab-org/labm"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gitlab-org/labm/Changelog.MD"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.1.4", ">= 6.1.4.7"
  spec.add_dependency "pg", "~> 1.1"
  spec.add_dependency "state_machines-activerecord", "~> 0.8.0"
  spec.add_development_dependency "rspec-rails", "~> 5.0.1"
  spec.add_development_dependency "factory_bot_rails", "~> 6.2.0"
  spec.add_development_dependency "shoulda-matchers", "~> 5.0"
end
