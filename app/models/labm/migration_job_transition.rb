# frozen_string_literal: true

module Labm
  class MigrationJobTransition < ApplicationRecord
    self.table_name = :batched_background_migration_job_transition_logs

    belongs_to :migration_job, foreign_key: :batched_background_migration_job_id

    validates :previous_status, :next_status, :migration_job, presence: true

    validates :exception_class, length: { maximum: 100 }
    validates :exception_message, length: { maximum: 1000 }

    enum previous_status: MigrationJob.state_machine.states.map(&:name), _prefix: true
    enum next_status: MigrationJob.state_machine.states.map(&:name), _prefix: true
  end
end
