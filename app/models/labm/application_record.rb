module Labm
  class ApplicationRecord < ActiveRecord::Base
    include EachBatch

    self.abstract_class = true

    # this allows to limit connections to which the model will connect
    class_attribute :limit_connection_names, default: nil

    class << self
      def using_connection(connection)
        previous_connection = self.overriding_connection

        unless previous_connection.nil? || previous_connection.equal?(connection)
          raise 'cannot nest connection overrides for models with configurable connections'
        end

        self.overriding_connection = connection

        yield
      ensure
        self.overriding_connection = nil unless previous_connection.equal?(self.overriding_connection)
      end

      def connection
        if connection = self.overriding_connection
          connection
        else
          super
        end
      end

      private

      def overriding_connection
        Thread.current[:overriding_connection]
      end

      def overriding_connection=(connection)
        Thread.current[:overriding_connection] = connection
      end
    end

    def connection_db_config
      self.class.connection_db_config
    end
  end
end
