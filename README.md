# Labm
Currently a Rails engine that extracts the implementation batched background migrations from GitLab Rails.

Hopefully the future home of other database tooling, such as:
  * automatic partition management
  * automatic database reindexing
  * general migration helpers which augment the helpers provided by Rails

## Usage
See the batched background migration documentation: https://docs.gitlab.com/ee/development/batched_background_migrations.html

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'labm'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install labm
```

## Development

Clone the repository:
```bash
$ git clone https://gitlab.com/gitlab-org/database-team/labm.git
```

Move into the project directory:
```bash
$ cd labm
```

Setup the database:
```bash
$ rails db:setup
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
