# frozen_string_literal: true

module Labm
  class MigrationWrapper
    def initialize(connection:)
      @connection = connection
    end

    # Wraps the execution of a background migration.
    #
    # Updates the job's tracking records with the status of the migration
    # when starting and finishing execution, and optionally saves batch_metrics
    # the migration provides, if any are given.
    #
    # By default, the job's batch_metrics are serialized to JSON for storage
    # in the database. Custom code can be configured in the engine to track metrics
    # in the parent application.
    def perform(job_tracking_record)
      job_tracking_record.run!

      execute_batch(job_tracking_record)

      job_tracking_record.succeed!
    rescue Exception => error
      job_tracking_record.failure!(error: error)

      raise
    ensure
      ::Labm.track_job_metrics(job_tracking_record)
    end

    private

    attr_reader :connection

    def execute_batch(tracking_record)
      job_instance = execute_job(tracking_record)

      if job_instance.respond_to?(:batch_metrics)
        tracking_record.metrics = job_instance.batch_metrics
      end
    end

    def execute_job(tracking_record)
      job_class = tracking_record.migration_job_class

      if job_class < Labm::MigrationJobs::BatchedMigrationJob
        execute_batched_migration_job(job_class, tracking_record)
      else
        execute_legacy_job(job_class, tracking_record)
      end
    end

    def execute_batched_migration_job(job_class, tracking_record)
      job_instance = job_class.new(
        start_id: tracking_record.min_value,
        end_id: tracking_record.max_value,
        batch_table: tracking_record.migration_table_name,
        batch_column: tracking_record.migration_column_name,
        sub_batch_size: tracking_record.sub_batch_size,
        pause_ms: tracking_record.pause_ms,
        connection: connection)

      job_instance.perform(*tracking_record.migration_job_arguments)

      job_instance
    end

    def execute_legacy_job(job_class, tracking_record)
      job_instance = job_class.new

      job_instance.perform(
        tracking_record.min_value,
        tracking_record.max_value,
        tracking_record.migration_table_name,
        tracking_record.migration_column_name,
        tracking_record.sub_batch_size,
        tracking_record.pause_ms,
        *tracking_record.migration_job_arguments)

      job_instance
    end
  end
end
