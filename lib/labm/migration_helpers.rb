# frozen_string_literal: true

module Labm
  module MigrationHelpers
    BATCH_SIZE = 1_000 # Number of rows to process per job
    SUB_BATCH_SIZE = 100 # Number of rows to process per sub-batch
    BATCH_CLASS_NAME = 'PrimaryKeyBatchingStrategy' # Default batch class for batched migrations
    BATCH_MIN_VALUE = 1 # Default minimum value for batched migrations
    BATCH_MIN_DELAY = 2.minutes.freeze # Minimum delay between batched migrations

    # Creates a background migration for the given table. A background migration runs one job
    # at a time, computing the bounds of the next batch based on the current migration settings and the previous
    # batch bounds. Each job's execution status is tracked in the database as the migration runs. The given job
    # class must be present in either the gem or the configured module, and the batch class (if specified) must also
    # be present in either the gem or the configured module.
    #
    # If migration with same job_class_name, table_name, column_name, and job_aruments already exists, this helper
    # will log a warning and skip the creation of the duplicate record.
    #
    # job_class_name - The background migration job class as a string
    # batch_table_name - The name of the table the migration will batch over
    # batch_column_name - The name of the column the migration will batch over
    # job_arguments - Extra arguments to pass to the job instance when the migration runs
    # job_interval - The pause interval between each job's execution, minimum of 2 minutes
    # batch_min_value - The value in the column the batching will begin at
    # batch_max_value - The value in the column the batching will end at, defaults to `SELECT MAX(batch_column)`
    # batch_class_name - The name of the class that will be called to find the range of each next batch
    # batch_size - The maximum number of rows per job
    # sub_batch_size - The maximum number of rows processed per "iteration" within the job
    #
    # *Returns the created Migration record*
    #
    # Example:
    #
    #     queue_batched_background_migration(
    #       'MyMigrationThatDoesWork',
    #       :events,
    #       :id,
    #       job_interval: 2.minutes,
    #       other_job_arguments: ['column1', 'column2'])
    #
    # Where the background migration exists:
    #
    #     class MyConfiguredModule::MyMigrationThatDoesWork
    #       def perform(start_id, end_id, batch_table, batch_column, sub_batch_size, *other_args)
    #         # do something
    #       end
    #     end
    def queue_batched_background_migration( # rubocop:disable Metrics/ParameterLists
      job_class_name,
      batch_table_name,
      batch_column_name,
      *job_arguments,
      job_interval:,
      batch_min_value: BATCH_MIN_VALUE,
      batch_max_value: nil,
      batch_class_name: BATCH_CLASS_NAME,
      batch_size: BATCH_SIZE,
      max_batch_size: nil,
      sub_batch_size: SUB_BATCH_SIZE
    )

      if Labm::Migration.for_configuration(job_class_name, batch_table_name, batch_column_name, job_arguments).exists?
        Labm.logger.warn "Background migration not enqueued because it already exists: " \
          "job_class_name: #{job_class_name}, table_name: #{batch_table_name}, column_name: #{batch_column_name}, " \
          "job_arguments: #{job_arguments.inspect}"
        return
      end

      job_interval = BATCH_MIN_DELAY if job_interval < BATCH_MIN_DELAY

      batch_max_value ||= connection.select_value(<<~SQL)
        SELECT MAX(#{connection.quote_column_name(batch_column_name)})
        FROM #{connection.quote_table_name(batch_table_name)}
      SQL

      status_event = batch_max_value.nil? ? :finish : :execute
      batch_max_value ||= batch_min_value

      migration = Labm::Migration.new(
        job_class_name: job_class_name,
        table_name: batch_table_name,
        column_name: batch_column_name,
        job_arguments: job_arguments,
        interval: job_interval,
        min_value: batch_min_value,
        max_value: batch_max_value,
        batch_class_name: batch_class_name,
        batch_size: batch_size,
        sub_batch_size: sub_batch_size,
        status_event: status_event
      )

      # Below `Migration` attributes were introduced after the initial table was created,
      # so any migrations that ran relying on initial table schema would not know
      # about columns introduced later on because this model is not
      # isolated in migrations, which is why we need to check for existence
      # of these columns first.
      if migration.respond_to?(:max_batch_size)
        migration.max_batch_size = max_batch_size
      end

      if migration.respond_to?(:total_tuple_count)
        # We keep track of the estimated number of tuples to reason later
        # about the overall progress of a migration.
        tuple_count = estimated_tuple_count(batch_table_name)
        migration.total_tuple_count = tuple_count if tuple_count >= 1
      end

      migration.save!
      migration
    end

    private

    def estimated_tuple_count(table_name)
      connection.select_value(<<~SQL).to_i
        SELECT reltuples
        FROM pg_class
        WHERE oid = #{connection.quote(table_name)}::regclass
      SQL
    end
  end
end
