# load this first, so state machines are enabled before loading the models
require 'state_machines-activerecord'

module Labm
  class Engine < ::Rails::Engine
    isolate_namespace Labm

    config.eager_load_paths.push(config.root.join('lib'))

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_bot
      g.factory_bot dir: 'spec/factories'
    end
  end
end
