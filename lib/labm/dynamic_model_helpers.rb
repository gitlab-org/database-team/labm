# frozen_string_literal: true

module Labm
  module DynamicModelHelpers
    BATCH_SIZE = 1_000

    def define_batchable_model(table_name, connection:)
      explicit_connection = connection

      Class.new(ActiveRecord::Base) do
        include EachBatch

        self.table_name = table_name
        self.inheritance_column = :_type_disabled

        def connection
          explicit_connection
        end
      end
    end
  end
end
