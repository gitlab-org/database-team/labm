# frozen_string_literal: true

module Labm
  module TestSupport
    FACTORY_PATH = File.expand_path('../../spec/factories', __dir__)
  end
end
