require "labm/version"
require "labm/engine"

module Labm
  SplitAndRetryError = Class.new(StandardError)

  mattr_accessor :logger, default: Rails.logger
  mattr_accessor :exception_tracker
  mattr_accessor :metrics_tracker
  mattr_accessor :feature_toggle

  mattr_accessor :migration_job_namespace
  mattr_accessor :batching_strategy_namespace

  def self.track_exception(exception, **extra)
    return unless exception_tracker

    exception_tracker.track_exception(exception, **extra)
  end

  def self.track_job_metrics(job_record)
    return unless metrics_tracker

    metrics_tracker.track_job_metrics(job_record)
  end

  def self.feature_enabled?(name, **extra)
    return true unless feature_toggle

    feature_toggle.enabled?(name)
  end

  def self.load_migration_job(class_name)
    if migration_job_namespace.nil?
      raise ArgumentError, 'migration_job_namespace is not configured'
    elsif migration_job_namespace.const_defined?(class_name)
      migration_job_namespace.const_get(class_name)
    end
  end

  def self.load_batching_strategy(class_name)
    if batching_strategy_namespace.nil?
      raise ArgumentError, 'batching_strategy_namespace is not configured'
    elsif batching_strategy_namespace.const_defined?(class_name)
      batching_strategy_namespace.const_get(class_name)
    end
  end
end
